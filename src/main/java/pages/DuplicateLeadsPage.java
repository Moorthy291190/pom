package pages;

import libraries.Annotations;

public class DuplicateLeadsPage extends Annotations {
 
	public  DuplicateLeadsPage typeCompanyName(String cName)
	{
		driver.findElementById("createLeadForm_companyName").clear();
		driver.findElementById("createLeadForm_companyName").sendKeys(cName);
		return this;
	}
    public ViewLeadsPage clickCreateLeadButton()
    {
    	
    	driver.findElementByName("submitButton").click();
    	return new ViewLeadsPage();
    }
}
