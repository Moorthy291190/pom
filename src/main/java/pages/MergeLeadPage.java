package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;

import libraries.Annotations;

public class MergeLeadPage extends Annotations{

	public Find2LeadPage clickFromLeadButton() throws InterruptedException
	{
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		Thread.sleep(2000);
		Set<String> windows = driver.getWindowHandles();
		List<String> wins=new ArrayList<String>(windows);
	driver.switchTo().window(wins.get(1));

		return new Find2LeadPage();
		
	}
	public Find2LeadPage clickToLeadButton() throws InterruptedException
	{
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		Set<String> windows1 = driver.getWindowHandles();
		List<String> wins1=new ArrayList<String>(windows1);
		driver.switchTo().window(wins1.get(1));
		Thread.sleep(2000);
		return new Find2LeadPage();
		
	}
	
	public ViewLeadsPage clickMergeButton()
	{
		driver.findElementByClassName("buttonDangerous").click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		return new ViewLeadsPage(); 
	}
	
}
