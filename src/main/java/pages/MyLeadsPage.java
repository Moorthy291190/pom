package pages;

import libraries.Annotations;

public class MyLeadsPage extends Annotations {
	
	public CreateLeadsPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead")
		.click();
		return new CreateLeadsPage();
		
	}
	public FindLeadsPage clickFindLeadTab()
	{
		driver.findElementByLinkText("Find Leads").click();
		return new FindLeadsPage();
		}
	
public MergeLeadPage clickMergeLeadTab()
{
	driver.findElementByLinkText("Merge Leads").click();
	return new MergeLeadPage();
			}

}
