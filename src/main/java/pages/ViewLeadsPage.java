package pages;

import libraries.Annotations;

public class ViewLeadsPage extends Annotations {
	
	public ViewLeadsPage verifyFirstName() {
		String text = driver.findElementById("viewLead_firstName_sp")
		.getText();
		if(text.equals("Sethu")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
	}
	public MyLeadsPage clickDeleteButton()
	{
		driver.findElementByLinkText("Delete").click();
		return new MyLeadsPage();
	}
	public DuplicateLeadsPage clickDuplicateButton()
	{
		driver.findElementByLinkText("Duplicate Lead").click();
		return new DuplicateLeadsPage();
	}
	
	public EditLeadsPage clickEditButton()
	{
		driver.findElementByLinkText("Edit").click();
		return new EditLeadsPage();
	}
	
	
	
	
}
