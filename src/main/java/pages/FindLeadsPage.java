package pages;


import libraries.Annotations;

public class FindLeadsPage extends Annotations {
public FindLeadsPage TypeFirstName(String fname)
{
	driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(fname);
	
	return this;
	}

public FindLeadsPage clickFindLeadButton()
{
	driver.findElementByXPath("//button[text()='Find Leads']").click();
	return this;
	}
public ViewLeadsPage clickLeadID() throws InterruptedException
{
	Thread.sleep(2000);
	driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
	return new ViewLeadsPage() ;
	}

}