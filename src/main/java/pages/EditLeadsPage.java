package pages;

import libraries.Annotations;

public class EditLeadsPage extends Annotations {

	public EditLeadsPage typeCompanyname(String cName)
	{
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys(cName);
		return new EditLeadsPage();
	}
	public ViewLeadsPage clickUpdateButton()
	{
		driver.findElementByXPath("(//input[@name='submitButton'])[1]").click();
		return new ViewLeadsPage();
	}
	
}
