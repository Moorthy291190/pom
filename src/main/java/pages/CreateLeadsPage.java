package pages;

import libraries.Annotations;

public class CreateLeadsPage extends Annotations {
	
	public CreateLeadsPage typeCompanyName(String cName) {
		driver.findElementById("createLeadForm_companyName")
		.sendKeys(cName);
		return this;
	}
	
	public CreateLeadsPage typeFirstName(String fName) {
		driver.findElementById("createLeadForm_firstName")
		.sendKeys(fName);
		return this;
	}
	
	public CreateLeadsPage typeLastName(String lName) {
		driver.findElementById("createLeadForm_lastName")
		.sendKeys(lName);
		return this;
	}
	
	public ViewLeadsPage clickCreateLeadButton() {
		driver.findElementByClassName("smallSubmit")
		.click();
		return new ViewLeadsPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
