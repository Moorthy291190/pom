package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import libraries.Annotations;

public class Find2LeadPage extends Annotations {
	public Find2LeadPage TypeFirst2Name(String fname) throws InterruptedException
	{
		driver.findElementByName("firstName").sendKeys(fname);
		Thread.sleep(2000);
		return this;
		}
	public Find2LeadPage click2FindLeadButton() throws InterruptedException
	{		
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		Thread.sleep(2000);
		return this;
		}
	public MergeLeadPage Click2LeadID() throws InterruptedException
	{
		
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		Thread.sleep(2000);
		Set<String> windows = driver.getWindowHandles();
		List<String>wins=new ArrayList<String>(windows);
		driver.switchTo().window(wins.get(0));
		
		return new MergeLeadPage();
		}


}
