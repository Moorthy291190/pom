package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC004_EditLead extends Annotations {
@BeforeClass
	public void setData()
	{
		excelFileName="TC004";
	}
	@Test(dataProvider="fetchData")
	public void EditLeadTest(String fname,String cName) throws InterruptedException
	{
		new MyHomePage()
		.clickLeadsTab()
		.clickFindLeadTab()
		.TypeFirstName(fname)
		.clickFindLeadButton()
		.clickLeadID()
		.clickEditButton()
		.typeCompanyname(cName)
		.clickUpdateButton();
		
	}
}
