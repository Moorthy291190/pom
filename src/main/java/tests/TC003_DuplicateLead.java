package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC003_DuplicateLead extends Annotations {

	 @BeforeClass
	 public void setDate()
	 {
		 excelFileName = "TC003"; 
	 }
	 @Test(dataProvider="fetchData")
	 public void DuplicateLeadTest(String fname,String cName) throws InterruptedException
	 {
		 new MyHomePage()
		 .clickLeadsTab()
		 .clickFindLeadTab()
		 .TypeFirstName(fname)
		 .clickFindLeadButton()
		 .clickLeadID()
		 .clickDuplicateButton()
		 .typeCompanyName(cName)
		 .clickCreateLeadButton();
		 
	 }
	
}
